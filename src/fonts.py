"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame

pygame.font.init()

# fonts
# small_font = pygame.font.SysFont("comicsansms",25)
# medium_font = pygame.font.SysFont("comicsansms",50)
# large_font = pygame.font.SysFont("comicsansms",75)


small_font = pygame.font.SysFont("freesans",25)
medium_font = pygame.font.SysFont("freesans",50)
large_font = pygame.font.SysFont("freesans",75)