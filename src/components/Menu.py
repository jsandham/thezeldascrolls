"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame
from colors import *
from fonts import *
from tools import *
from Component import *


# -----------------------------------------------------------------|     _
# |      |                                                         |     |
# |      dy                                                        |     |
# |      |                                                         |     |
# |--dx--*--------||------------------------------------------|    |     |
# |      |        ||                                          |    |     |
# |      |        ||  text label/surface label goes here      |    |     |
# |      |        ||                                          |    |     |
# |      |--------||------------------------------------------|    |     | 
# |                                |                               |     |
# |                                dz                              |     |
# |                                |                               |   height
# |      |--------||------------------------------------------|    |     |
# |      |        ||                                          |    |     |
# |      |        ||  text label/surface label goes here      |    |     |
# |      |        ||                                          |    |     |
# |      |--------||------------------------------------------|    |     |
# |                                                                |     |
# |      selector_width            label_width                     |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |                                                                |     |
# |----------------------------------------------------------------|     -
#    
# |-------------------------- width--------------------------------|



class Menu(Component):
	"""
	The Menu class is used to create player interactable menus in the game. Each
	menu option (called a label) can be either a text string or a pygame surface.
	Whe creating an istance of this class you must provide an image filename to 
	a menu selector image and an image filename to a menu background image.
	"""
	def __init__(self, selector_image_filename, menu_image_filename, max_labels = 4):
		self.selector_visible_flag = True
		self.selector_image = pygame.image.load(selector_image_filename)
		self.menu_image = pygame.image.load(menu_image_filename)

		self.max_labels = max_labels
		self.number_of_labels = 0
		self.number_of_visible_labels = 0
		self.active_label = 0
		self.visible_index = 0

		self.labels = []
		self.visible_labels = []

	def selector_visible(self):
		"""
		Sets menu selector to visible.
		"""
		self.selector_visible_flag = True

	def selector_invisible(self):
		"""
		Sets the menu selector to ivisible.
		"""
		self.selector_visible_flag = False

	def add(self, label_list):
		"""
		Add a list of labels to the menu. If the menu already has some labels, 
		than the new list is simply appended to the existig one.
		"""
		self.active_label = 1
		self.visible_index = 1
		self.number_of_labels += len(label_list)
		self.number_of_visible_labels = min(self.max_labels, self.number_of_labels)
		self.labels += label_list

		self.visible_labels = []
		for i in range(0, self.number_of_visible_labels):
			self.visible_labels.append(self.labels[i])

	def set(self, label_list):
		"""
		Sets the menu labels to the input label list. If the menu already has 
		some labels, than they are overwriten to the new label list.
		"""
		self.active_label = 1
		self.visible_index = 1
		self.number_of_labels = len(label_list)
		self.number_of_visible_labels = min(self.max_labels, self.number_of_labels)
		self.labels = label_list

		self.visible_labels = []
		for i in range(0, self.number_of_visible_labels):
			self.visible_labels.append(self.labels[i])


	def clear(self):
		"""
		Clears the menu of all labels and set the menu selector to be invisible.
		"""
		self.selector_visible_flag = False
		self.active_label = 0
		self.visible_index = 0
		self.number_of_labels = 0
		self.number_of_visible_labels = 0
		self.labels = []
		self.visible_labels = []

	def active(self):
		"""
		Returns the currently active menu label.
		"""
		if self.selector_visible_flag == True:
			return self.labels[self.active_label-1]
		else:
			return None

	def active_index(self):
		"""
		Returns the currently active menu label index.
		"""
		return self.active_label-1

	def size(self):
		"""
		Returns the number of labels in menu.
		"""
		return self.number_of_labels

	def update(self, event):
		"""
		Update menu based on player keyboard up/down arrow key input.
		"""
		if self.selector_visible_flag == True:
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_UP:
					self.up()
				elif event.key == pygame.K_DOWN:
					self.down()

	def up(self):
		"""

		"""
		self.active_label = max(self.active_label - 1, 1)
		if self.active_label < self.visible_index:
			self.visible_index -= 1

		for i in range(1, self.number_of_visible_labels + 1):
			self.visible_labels[i-1] = self.labels[self.visible_index - 1 + i - 1]

	def down(self):
		"""

		"""
		self.active_label = min(self.active_label + 1, self.number_of_labels)
		if self.active_label >= self.visible_index + self.max_labels:
			self.visible_index += 1

		for i in range(1, self.number_of_visible_labels + 1):
			self.visible_labels[i-1] = self.labels[self.visible_index -1 + i - 1]

	def draw(self, display_window, location, width, height, dx, dy, dz, selector_width, label_width):
		"""
		Draw menu to the input display window at the specified location and 
		input menu size info.
		"""
		# find top left corner to blit image based on location
		x = location[0] - width/2
		y = location[1] - height/2

		# blit menu image to screen
		self.menu_image = pygame.transform.scale(self.menu_image, (width, height))
		display_window.blit(self.menu_image, (x,y))

		# blit menu selector
		if self.selector_visible_flag == True: 
			self.selector_image = pygame.transform.scale(self.selector_image, (selector_width, selector_width))
			temp = self.active_label - self.visible_index
			display_window.blit(self.selector_image, (x + dx, y + dy + temp*(selector_width+dz)))

		# blit visible labels
		for i in range(1, self.number_of_visible_labels+1):
			item_location_x = x + dx + selector_width + label_width/2
			item_location_y = y + dy + selector_width/2 + (i-1)*(selector_width + dz)

			# draw labels depending on whether they are text strings or pygame surfaces
			if isinstance(self.visible_labels[i-1], basestring):
				message_to_screen(self.visible_labels[i-1], black, display_window, item_location_x, item_location_y)
			elif isinstance(self.visible_labels[i-1], pygame.Surface):
				display_window.blit(self.visible_labels[i-1], (item_location_x, item_location_y))


class SpriteMenu(Component):
	"""
	The SpriteMenu class is used to create player interactable menus in the game 
	similar to the Menu class except there is no menu background and each label is a 
	pygame sprite (which has an associated image attached to it). 
	"""
	def __init__(self, selector_image_filename):
		self.selector_visible_flag = True
		self.selector_image = pygame.image.load(selector_image_filename)
	
		self.number_of_sprites = 0
		self.active_sprite = 0

		self.sprites = []

	def selector_visible(self):
		"""
		Sets menu selector to visible.
		"""
		self.selector_visible_flag = True

	def selector_invisible(self):
		"""
		Sets the menu selector to ivisible.
		"""
		self.selector_visible_flag = False

	def add(self, sprite_list):
		"""
		Add a list of sprites to the menu. If the menu already has some sprites, 
		than the new list is simply appended to the existig one.
		"""
		self.active_sprite = 1
		self.number_of_sprites += len(sprite_list)
		self.sprites += sprite_list

	def set(self, sprite_list):
		"""
		Sets the menu sprites to the input sprites list. If the menu already has 
		some sprites, than they are overwriten to the new sprite list.
		"""
		self.active_sprite = 1
		self.number_of_sprites = len(sprite_list)
		self.sprites = sprite_list

	def remove(self, sprite):
		index = 0
		for spt in self.sprites:
			if spt == sprite:
				self.number_of_sprites -= 1
				if index < self.active_sprite - 1:
					self.active_sprite -= 1	
				del self.sprites[index]
				break
			index += 1

	def clear(self):
		"""
		Clears the menu of all sprites and set the menu selector to be invisible.
		"""
		self.selector_visible_flag = False
		self.active_sprite = 0
		self.number_of_sprites = 0
		self.sprites = []

	def active(self):
		"""
		Returns the currently active menu sprite.
		"""
		if self.selector_visible_flag == True:
			return self.sprites[self.active_sprite-1]
		else:
			return None

	def active_index(self):
		"""
		Returns the currently active menu sprite index.
		"""
		return self.active_sprite-1

	def size(self):
		"""
		Returns the number of sprites in menu.
		"""
		return self.number_of_sprites

	def update(self, event):
		"""
		Update menu based on player keyboard up/down arrow key input.
		"""
		if self.selector_visible_flag == True:
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_UP:
					self.up()
				elif event.key == pygame.K_DOWN:
					self.down()

	def up(self):
		"""

		"""
		self.active_sprite = max(self.active_sprite - 1, 1)

	def down(self):
		"""

		"""
		self.active_sprite = min(self.active_sprite + 1, self.number_of_sprites)

	def draw(self, display_window, selector_width):
		"""
		Draw menu to the input display window at the specified location and 
		input menu size info.
		"""
		if self.number_of_sprites >= 1:
			# find location of menu selector
			x = self.sprites[self.active_sprite - 1].rect.x - selector_width
			y = self.sprites[self.active_sprite - 1].rect.y

			# draw selector
			if self.selector_visible_flag == True: 
				self.selector_image = pygame.transform.scale(self.selector_image, (selector_width, selector_width))
				display_window.blit(self.selector_image, (x, y))

		# draw sprites
		for sprite in self.sprites:
			sprite.image = pygame.transform.scale(sprite.image, (128, 128))
			display_window.blit(sprite.image, (sprite.rect.x, sprite.rect.y))


# -----------------------------------------------------------------|     _
# |      |                                                         |     |
# |      dy                                                        |     |
# |      |                                                         |     |
# |--dx--*----------------------------------------------------|    |     |
# |      |                                                    |    |     |
# |      |                                                    |    |     |
# |      |                                                    |    |     |
# |      |         Text information goes here...              |    |     | 
# |      |                                                    |    |     |
# |      |                                                    |    |     |
# |      |                                                    |    |   height
# |      |                                                    |    |     |
# |      |----------------------------------------------------|    |     |
# |                                |                               |     |
# |                                dz                              |     |
# |                                |     |selector||--text width-| |     |
# |                                        width                   |     |
# |       |-------||--------------|      |-------||--------------| |     |
# |       |       ||              |      |       ||              | |     |
# |       |       ||  text label  |      |       ||  text label  | |     |
# |       |       ||              |      |       ||              | |     |
# |       |-------||--------------|      |-------||--------------| |     |
# |                                                                |     |
# |----------------------------------------------------------------|     -
#    
# |-------------------------- width--------------------------------|
class BinaryInfoMenu(Component):
	def __init__(self, selector_image_filename, menu_image_filename):
		self.selector_visible_flag = True
		self.selector_image = pygame.image.load(selector_image_filename)
		self.menu_image = pygame.image.load(menu_image_filename)

		self.number_of_labels = 0
		self.active_label = 0

		self.labels = []
		self.selectable_labels = []
		self.text_info = None

	def selector_visible(self):
		"""
		Sets menu selector to visible.
		"""
		self.selector_visible_flag = True

	def selector_invisible(self):
		"""
		Sets menu selector to invisible.
		"""
		self.selector_visible_flag = False

	def add(self, label_list):
		"""
		Add a list of two labels to the menu. The input label list must have 
		exactly two elements.
		"""
		if len(label_list) != 2:
			print "ERROR: BinaryInfoMenu takes exactly two text labels"
		else:
			self.active_label = 1
			self.number_of_labels = len(label_list)
			self.labels = label_list
			self.selectable_labels = 2 * [True]

	def set(self, label_list):
		"""
		Set a list of two labels to the menu. The input label list must have 
		exactly two elements. This does the same thing as the add method.
		"""
		if len(label_list) != 2:
			print "ERROR: BinaryInfoMenu takes exactly two text labels"
		else:
			self.active_label = 1
			self.number_of_labels = len(label_list)
			self.labels = label_list
			self.selectable_labels = 2 * [True]

	# def set_text(self, text):
		

	def clear(self):
		"""
		Clears the menu of all labels and set the menu selector to be invisible.
		"""
		self.selector_visible_flag = False
		self.active_label = 0
		self.number_of_labels = 0
		self.labels = []
		self.selectable_labels = []
		self.text_info = None

	def set_label_unselectable(self, index):
		"""
		Set the label at the input index (i.e. o or 1) to be unselectable by player.
		"""
		if index < self.number_of_labels:
			self.selectable_labels[index] = False 
		else:
			print "ERROR: BinaryInfoMenu takes exactly two test labels"

	def active(self):
		"""
		Returns the currently active menu label.
		"""
		if self.selector_visible_flag == True:
			if self.selectable_labels[self.active_label-1] == True:
				return self.labels[self.active_label-1]
			else:
				return None
		else:
			return None

	def active_index(self):
		"""
		Returns the currently active menu label index.
		"""
		return self.active_label-1

	def size(self):
		"""
		Returns the number of labels in menu (must be two).
		"""
		return self.number_of_labels

	def update(self, event):
		"""
		Update menu based on player keyboard left/right arrow key input.
		"""
		if self.selector_visible_flag == True:
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_LEFT:
					self.left()
				elif event.key == pygame.K_RIGHT:
					self.right()

	def left(self):
		"""

		"""
		self.active_label = max(self.active_label - 1, 1)

	def right(self):
		"""

		"""
		self.active_label = min(self.active_label + 1, self.number_of_labels)

	def draw(self, display_window, location, width, height, dx, dy, dz, selector_width, label_width):
		"""
		Draw menu to the input display window at the specified location and 
		input menu size info.
		"""
		# find top left corner to blit image based on location
		x = location[0] - width/2
		y = location[1] - height/2

		# blit menu image to screen
		self.menu_image = pygame.transform.scale(self.menu_image, (width, height))
		display_window.blit(self.menu_image, (x,y))

		# blit menu selector
		if self.selector_visible_flag == True: 
			self.selector_image = pygame.transform.scale(self.selector_image, (selector_width, selector_width))
			temp = self.active_label - 1
			display_window.blit(self.selector_image, (x + dx + temp*(selector_width + label_width + dz), y + dy + dz))

		# blit labels
		for i in range(1, self.number_of_labels+1):
			item_location_x = x + dx + selector_width + label_width/2 + (i-1)*(selector_width + label_width + dz)
			item_location_y = y + dy + dz

			font_color = black
			if self.selectable_labels[i-1] == False:
				font_color = gray

			# draw labels 
			message_to_screen(self.labels[i-1], font_color, display_window, item_location_x, item_location_y)