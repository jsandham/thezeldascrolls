#!/usr/bin/env python
"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"
    
import sys
sys.path.insert(0, '/home/james/Documents/thezeldascrolls/src/states')
sys.path.insert(0, '/home/james/Documents/thezeldascrolls/src/characters')
sys.path.insert(0, '/home/james/Documents/thezeldascrolls/src/components')
sys.path.insert(0, '/home/james/Documents/thezeldascrolls/src/items')

import pygame
# import main
from main import *


if __name__ =='__main__':
    # setup.GAME
    main()
    pygame.quit()
    sys.exit()
