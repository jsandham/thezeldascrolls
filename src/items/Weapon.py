"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame
from Item import *


class Weapon(Item):
	"""
	Weapon class
	"""
	def __init__(self, image_filename, container, activation, name, description, cost, weight, level, modifiers, power):
		super(Weapon, self).__init__(image_filename, container, activation, name, description, cost, weight, level, modifiers)
		self._power = power

	def power(self):
		"""
		Return weapon power
		"""
		return self._power
