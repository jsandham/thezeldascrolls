"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame
from Item import *


class Potion(Item):
	"""
	Potion class
	"""
	def __init__(self, image_filename, container, activation, name, description, cost, weight, level, modifiers, potency):
		super(Potion, self).__init__(image_filename, container, activation, name, description, cost, weight, level, modifiers)
		self._potency = potency

	def potency(self):
		"""
		Return potion potency
		"""
		return self._potency

