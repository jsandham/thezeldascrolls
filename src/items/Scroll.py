"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame
from Item import *
from Spell import *


class Scroll(Item):
	"""
	Magical scroll class
	"""
	def __init__(self, image_filename, container, activation, name, description, cost, weight, level, modifiers, intensity):
		super(Scroll, self).__init__(image_filename, container, activation, name, description, cost, weight, level, modifiers)
		self._intensity = intensity
		self._spell = Spell("../artwork/lightning.png", (7,1)) 

	def intensity(self):
		"""
		Return spell intensity
		"""
		return self._intensity

	def spell(self):
		"""
		Return spell
		"""
		return self._spell