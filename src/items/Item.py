"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame
from tools import *


class Item(pygame.sprite.Sprite):
	"""
	Item class
	"""
	def __init__(self, image_filename, container, activation, name, description, cost, weight, level, modifiers):
		pygame.sprite.Sprite.__init__(self)
		self.image = pygame.image.load(image_filename)
		self._container = container
		self._activation = activation
		self._name = name
		self._description = description
		self._cost = cost
		self._weight = weight
		self._level = level
		self._modifiers = modifiers

	def container(self):
		"""
		Return container that item is located in
		"""
		return self._container

	def activation(self):
		"""
		Return activation keyword
		"""
		return self._activation

	def name(self):
		"""
		Return item name
		"""
		return self._name

	def description(self):
		"""
		Return item string description
		"""
		return self._description

	def cost(self):
		"""
		Return item cost
		"""
		return self._cost

	def weight(self):
		"""
		Return item weight
		"""
		return self._weight

	def level(self):
		"""
		Return item level
		"""
		return self._level

	def modifier_keys(self):
		"""
		Return list of modifier keys
		"""
		return self._modifiers.keys()

	def modifier_values(self):
		"""
		Return list of modifier values
		"""
		return self._modifiers.values()

	def modifier(self, stat):
		"""
		Return modifier corresponding to the input stat
		"""
		return self._modifiers[stat]
