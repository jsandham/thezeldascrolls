import pygame
import setup
import random
import pytmx
from colors import *
from tools import *
from constants import *
from State import *
from Component import *
from NPC import *
from DialogBox import *
from Quest import *
from Stats import *


class World(State):
	def __init__(self, world_characters, world_components, world_tmx_map, start_location, prev, curr, next, battle = False):
		super(World, self).__init__(world_characters, world_components, prev, curr, next)
		self.world_map = world_tmx_map
		self.world_mini_map = self.make_mini_map(world_tmx_map)
		self.world_shift = (0, 0) 
		self.last_player_position = start_location

		self.battle_flag = battle
		self.dialog_flag = False
		self.mini_map_flag = False


	def enter_state(self):
		setup.player.reposition(self.last_player_position)
		setup.player.restate('resting')
		self.active = self.current
		self.dialog_flag = False
		self.mini_map_flag = False
	
	def leave_state(self):
		self.last_player_position = setup.player.position()

	def process_event(self, event):
		self.update(event)

	def active_state(self):
		return self.active

	# update all (non-playable) characters
	def update_characters(self, event):
		for key in self.characters:
			if setup.player.beside(self.characters[key]) == False:
				self.characters[key].update(event)

	# update (playable) character
	def update_player(self, event):
		if self.active_component != None:
			if self.active_component.active == False:
				setup.player.update(event)
		else:
			setup.player.update(event)

	# update active component
	def update_components(self, event):
		for key in self.components:
			if self.components[key].active == True:
				self.components[key].update(event)
				break

	# update world
	def update(self, event):
		if event != None:
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_ESCAPE:
					pygame.quit()
					quit()
				elif event.key == pygame.K_TAB:
					self.active = 'Stats'
				elif event.key == pygame.K_m:
					self.mini_map_flag = not self.mini_map_flag
				elif event.key == pygame.K_RETURN:
					self.interact_with_characters(event)

		self.update_characters(event)
		self.update_player(event)
		self.update_components(event)
		self.shift_world()
		self.check_if_entering_battle()
		self.check_if_exiting_world()

	# draw characters
	def draw_characters(self, display):
		for key in self.characters:
			self.characters[key].draw(display, self.world_shift)

	# draw player
	def draw_player(self, display):
		sx = setup.player.rect.x
		sy = setup.player.rect.y
		width = self.world_map.width
		height = self.world_map.height
		if sx < 384: 
			sx = 0
		elif sx > width*32-384:
			sx = setup.player.rect.x - 384 - (setup.player.rect.x - (width*32-384))
		else:
			sx = setup.player.rect.x - 384

		if sy < 384: 
			sy = 0
		elif sy > height*32-384:
			sy = setup.player.rect.y - 384 - (setup.player.rect.y - (height*32-384))
		else:
			sy = setup.player.rect.y - 384
		setup.player.draw(display, (sx, sy))

	# draw map
	def draw_map(self, display):
		world_surface = pygame.Surface((800+32, 800+32))

		layer_index = 0
		for layer in self.world_map.visible_layers:

			# draw map tile layers
			if isinstance(layer, pytmx.TiledTileLayer):
				for x in range(0, 25+1):
					for y in range(0, 25+1):
						image = self.world_map.get_tile_image(self.world_shift[0]/32 + x, self.world_shift[1]/32 + y, layer_index)
						if image != None:
							world_surface.blit(image, (32*x, 32*y))
			layer_index += 1

		# crop surface for smooth transition while moving
		cropped_world_surface = pygame.Surface((800,800))
		cropped_world_surface.blit(world_surface, (0,0), (self.world_shift[0] % 32, self.world_shift[1] % 32, 800, 800))

		# blit world to screen
		display.blit(cropped_world_surface, (0,0))

	# draw "heads-up display"
	def draw_hud(self, display):
		display.blit(TextSurface("Time: ", "freesans", 15, white, 200), (20, 720))
		display.blit(TextSurface("World: " + self.active, "freesans", 15, white, 400), (20, 740))
		display.blit(TextSurface("X: " + str(setup.player.rect.x), "freesans", 15, white, 200), (20, 760))
		display.blit(TextSurface("Y: " + str(setup.player.rect.y), "freesans", 15, white, 200), (20, 780))

		display.blit(TextSurface("Gold: " + str(setup.player.gold()), "freesans", 15, white, 200), (700, 720))
		display.blit(TextSurface("Health: " + str(setup.player.health()), "freesans", 15, white, 200), (700, 740))
		display.blit(TextSurface("Mana: " + str(setup.player.mana()), "freesans", 15, white, 200), (700, 760))
		display.blit(TextSurface("XP: " + str(setup.player.xp()), "freesans", 15, white, 200), (700, 780))


	# draw mini-map
	def draw_mini_map(self, display):
		player_location_x = 800*setup.player.rect.x/(self.world_map.width*self.world_map.tilewidth)
		player_location_y = 800*setup.player.rect.y/(self.world_map.height*self.world_map.tileheight)
		display.blit(self.world_mini_map, (0,0))
		pygame.draw.circle(display, (255,0,0,0), (player_location_x, player_location_y), 5, 0)

	# draw world
	def draw(self, display):
		self.draw_map(display)
		self.draw_characters(display)
		self.draw_player(display)
		self.draw_hud(display)
		for key in self.components:
			if self.components[key].visible == True:
				self.components['DialogBox'].draw(display, (400, 600), 800, 400, 650, 50, 100, 50)
				break

		if self.mini_map_flag:
			self.draw_mini_map(display)

	def check_if_entering_battle(self):
		if self.battle_flag == True:
			if setup.player.new_tile == True:
				rand = random.randrange(1, 100)
				if rand <= 6:
					self.active = 'Battle'

	def check_if_exiting_world(self):
		for layer in self.world_map.visible_layers:
			if isinstance(layer, pytmx.TiledObjectGroup):
				if layer.name == "exit":
					for obj in layer:
						if pygame.Rect(obj.x, obj.y, obj.width, obj.height).contains(setup.player.rect) == True:
							self.active = self.previous
							break
				elif layer.name == "impassable":
					for obj in layer:
						if pygame.Rect(obj.x, obj.y, obj.width, obj.height).colliderect(setup.player.rect) == True:
							setup.player.reset_coordinates()
							break
				else:
					for obj in layer:
						if pygame.Rect(obj.x, obj.y, obj.width, obj.height).contains(setup.player.rect) == True:
							self.active = layer.name

	def shift_world(self):
		width = self.world_map.width
		height = self.world_map.height
		self.world_shift = (min((width - 26)*32, max(0, setup.player.rect.x - 384)), min((height - 26)*32, max(0, setup.player.rect.y - 384)))

	def interact_with_characters(self, event):
		# loop through every character in the world
		for key in self.characters:

			# check to see if player is beside character
			if setup.player.beside(self.characters[key]) == True:
				print "currently interacting with " + self.characters[key].name()
				print "quests in player quest log"
				for quest in setup.player.quests():
					print quest.title()
				print ""
				print "quests in npc quest log"
				for quest in self.characters[key].quests():
					print quest.title()

				# check to see if interacting with this character completes any quests in player quest log
				setup.player.update_quests(self.characters[key].name())
				
				# check to see if interacting with this character allows us to turn in any completed quests
				# setup.player.turnin_quests(self.characters[key].name())

				# check to see if interacting with this character adds any quests to player quest log 
				setup.player.accept_quests(self.characters[key].quests())
				self.characters[key].clear_quests()
				
				# set active component to DialogBox with xml dialog tree
				character_xml_tree = self.characters[key].xml_tree()
				self.components['DialogBox'].attach(character_xml_tree)
				self.active_component = self.components['DialogBox']
				break

	def make_mini_map(self, tmx_map):
		mini_map = pygame.Surface((tmx_map.width, tmx_map.height))
		for layer in tmx_map.visible_layers:
			if isinstance(layer, pytmx.TiledTileLayer):
				for x,y,image in layer.tiles():
					color = image.get_at((16, 16))
					mini_map.set_at((x, y), color)

		mini_map = pygame.transform.scale(mini_map, (SCREEN_SIZE[0], SCREEN_SIZE[1]))
		return mini_map
