import pygame
import copy
import random
import setup
from constants import *
from State import *
from Menu import *
from DialogBox import *
from Enemy import *
from GameOver import *


class Battle(State):
	def __init__(self, battle_characters, battle_components, prev, curr, next):
		super(Battle, self).__init__(battle_characters, battle_components, prev, curr, next)
		self.battle_image = SpriteSheet("../artwork/battle.png").get_image()
		self.battle_image = pygame.transform.scale(self.battle_image, (SCREEN_SIZE[0], SCREEN_SIZE[1]))

		self.current_message = None
		self.current_enemy = None
		self.current_spell = None

		self.flag = "enter"
		self.spell_flag = "no spell"

		self.main_menu = self.components['MainMenu']
		self.popup_menu = self.components['Popup']
		self.sprite_menu = self.components['SpriteMenu']
		self.message_box = self.components['MessageBox']
		self.surface_box = self.components['SurfaceBox']

	# intialize variable upon entering battle
	def enter_state(self):
		self.enemies = self.generate_enemies('scrapper', random.randint(1,3))
		self.current_message = self.welcome_message()
		self.current_enemy = self.enemies[0]
		self.current_spell = None
		self.active = self.current 
		self.flag = "enter"
		self.spell_flag = "no spell"

		self.main_menu.clear()
		self.popup_menu.clear()
		self.sprite_menu.clear()

		self.set_active_menu(self.sprite_menu, self.enemies)
		self.set_active_menu(self.main_menu, ["Attack", "Cast Spell", "Use Item", "Run"])
	
		self.message_box.reset_message()

	# reset variables upon leaving battle 
	def leave_state(self):
		pass

	# player died, set state to game over
	def player_died(self):
		self.active = self.next
		self.flag = "enter"

	def process_event(self, event):
		self.update(event)

	def active_state(self):
		return self.active

	def update(self, event):
		health_bar = Bar(120, 20, setup.player.health(), setup.player.max_health(), red)
		mana_bar = Bar(120, 20, setup.player.mana(), setup.player.max_mana(), blue)
		xp_bar = Bar(120, 20, setup.player.xp(), setup.player.max_xp(), purple)

		self.surface_box.update_surfacebox(["HP","Mana","XP"], [health_bar, mana_bar, xp_bar])
		self.message_box.update_message(self.current_message)
		self.flag = setup.player.animate_battle(self.flag)
		self.flag = self.current_enemy.animate_battle(self.flag)
		if self.spell_flag != "no spell":
			self.spell_flag = self.current_spell.animate_battle(self.spell_flag)

		if event != None:
			if event.type == pygame.KEYDOWN:
				self.active_menu.update(event)
				if event.key == pygame.K_ESCAPE:
					pygame.quit()
					quit()
				elif event.key == pygame.K_RETURN:
					self.process_key_return(self.active_menu.active())

		if self.flag == "exit battle":
			self.active = self.previous
		elif self.flag == "start attack":
			self.active_menu.selector_invisible()
		elif self.flag == "attack phase over":
			setup.player.take_physical_damage(self.current_enemy)
			self.active_menu = self.main_menu
			self.active_menu.selector_visible()
			self.flag = "walk right"
		elif self.flag == "player died":
			self.player_died()

	def process_key_return(self, key):
		if key == "Attack":
			self.current_spell = None
			self.current_message = self.attack_message()
			self.set_active_menu(self.sprite_menu, self.enemies)
		elif key == "Cast Spell":
			self.current_message = self.cast_spell_message()
			self.set_active_menu(self.popup_menu, setup.player.spells() + ["Back"])
		elif key == "Use Item":
			self.current_message = self.use_item_message()
			self.set_active_menu(self.popup_menu, setup.player.inventory() + ["Back"])
		elif key == "Run":
			self.current_message = self.run_message(0)
			self.active_menu.selector_invisible()
			self.flag = "leave"
		elif key == "Back":
			self.current_message = self.welcome_message()
			self.set_active_menu(self.main_menu, ["Attack", "Cast Spell", "Use Item", "Run"])
		elif key in setup.player.spells():
			self.current_spell = setup.player.sprite_dict['Spells'].get_item(key).get_spell()
			setup.player.use_spell(key)
			self.current_message = self.attack_message()
			self.set_active_menu(self.sprite_menu, self.enemies)
		elif key in setup.player.inventory():
			setup.player.use(key)
			self.set_active_menu(self.active_menu, setup.player.inventory() + ["Back"])
		elif key == None:
			pass
		else:
			for i in range(0,len(self.enemies)):
				print self.enemies[i].sprite_dict
			
			self.flag = "start attack"
			if self.current_spell != None:
				self.spell_flag = "spell"
			self.current_enemy = self.enemies[self.sprite_menu.active_index()]
			self.current_message = self.welcome_message()
			
			self.current_enemy.take_physical_damage(setup.player)

			if self.current_enemy.isDead() == True:
				setup.player.earn_xp(4)
				setup.player.earn_gold(4)
				del self.enemies[self.sprite_menu.active_index()]
				self.sprite_menu.remove(self.current_enemy)

				if self.sprite_menu.size() == 1:
					self.active_menu.clear()
					self.flag = "leave"
					self.current_message = self.run_message(0)
				else:
					self.current_enemy = self.enemies[self.sprite_menu.active_index()]
					self.current_message = self.enemy_killed()
					self.set_active_menu(self.main_menu, ["Attack", "Cast Spell", "Use Item", "Run"])

	def draw(self, display):
		display.blit(self.battle_image, (0, 0))
		self.main_menu.draw(display, (650, 650), 250, 300, 20, 20, 20, 50, 100) 
		self.sprite_menu.draw(display, 50) 
		if self.active_menu == self.popup_menu:
			self.active_menu.draw(display, SCREEN_CENTRE, 200, 250, 20, 20, 20, 50, 100)
		self.message_box.draw(display, (250,650), 500, 300)
		self.surface_box.draw(display, (150, 75), 300, 125, 100, 20, 20, 10)
		setup.player.draw(display)
		if self.spell_flag != "no spell":
			self.current_spell.draw(display)

	def welcome_message(self):
		return ["Select an option"]

	def attack_message(self):
		return ["Choose an enemy to attack..."]

	def cast_spell_message(self):
		return ["Choose spell..."]

	def use_item_message(self):
		return ["Choose item..."]

	def enemy_killed(self):
		return ["Enemy killed"]

	def run_message(self, xp):
		return ["Leaving battle", "You gained " + str(xp) + " XP"]

	def generate_enemies(self, enemy_name, number):
		enemies = []
		for i in range(0,number):
			dx = 0
			if i % 2 == 0:
				dx = 0
			else:
				dx = 128
			enemy = Enemy("../data/scrapper.xml", "../artwork/scrapper.png", (3,4))
			enemy.reposition((400+dx, 50+128*i))
			enemies.append(enemy)

		return enemies