"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame


class Container(object):
	"""

	"""
	def __init__(self):
		self.item_dict = {}
		self.quantity_dict = {}

	def add(self, label, item):
		"""
		Adds an item identified by its key to the container.
		"""
		self.item_dict[label] = item
		if label in self.quantity_dict:
			self.quantity_dict[label] += 1
		else:
			self.quantity_dict[label] = 1

	def remove(self, label):
		"""
		Removes and returns and item identified by its key from the container.
		"""
		item = None
		if label in self.quantity_dict:
			if self.quantity_dict[label] == 1:
				quantity = self.quantity_dict.pop(label, None)
				item = self.item_dict.pop(label, None)
			else:
				self.quantity_dict[label] -= 1
				item = self.item_dict[label]

		return item

	def get_item(self, label):
		"""
		Returns the item identified by the input key.
		"""
		item = None
		if label in self.item_dict:
			item = self.item_dict[label]
		return item

	def set_item(self, label, item):
		"""
		Sets the item identified by key in the container (with a quantity of 1).
		"""
		self.item_dict[label] = item
		self.quantity_dict[label] = 1

	def empty(self):
		"""
		Empties the container of all items.
		"""
		self.item_dict = {}
		self.quantity_dict = {}

	def quantity(self, label):
		"""
		Returns the quantity of the item identified by key.
		"""
		count = 0
		if label in self.item_dict:
			count = self.quantity_dict[label]
		return count

	def contains(self, label):
		"""
		Returns True if the given key is in the container, else returns False.
		"""
		if label in self.item_dict:
			return True
		else:
			return False

	def keys(self):
		"""
		Returns list of item_dict keys.
		"""
		return list(self.item_dict.keys())

	def values(self):
		"""
		Returns list of item_dict values.
		"""
		return list(self.item_dict.values())