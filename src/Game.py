"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame
import pickle
from constants import *
from setup import *
from colors import *
from fonts import *
from tools import *
from Player import *
from World import *
from Menu import *


class Game(object):
	"""
	Main Game class.
	"""
	def __init__(self):
		self.states = {}
		self.old_state = None
		self.active_state = 'Start'
		
	def set_states(self, states):
		self.states = states

	def change_state(self):
		if self.old_state != None:
			self.states[self.old_state].leave_state()
			print "leaving " + str(self.old_state) + " state"
			game_states['Stats'].previous = self.old_state
			
		if self.old_state == 'FastTravel':
			game_states['Overworld'].last_player_position = setup.player.fasttravel_end
		self.states[self.active_state].enter_state()
		print "entering " + str(self.active_state) + " state"

	def pause(self):
		pause = True

		pause_menu = Menu("../artwork/sword.png", "../artwork/scroll.png")
		pause_menu.add(["Return To Game", "Save Game", "Quit Game"])

		while pause:
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					pygame.quit()
					quit()
				if event.type == pygame.KEYDOWN:
					pause_menu.update(event)
					if event.key == pygame.K_ESCAPE:
						pygame.quit()
						quit()
					elif event.key == pygame.K_RETURN:
						if pause_menu.active() == "Return To Game":
							pause = False
						elif pause_menu.active() == "Save Game":
							pause = False
							file = open("save.p", 'w')
							# pickle.dump(setup.player_dict, file)
						elif pause_menu.active() == "Quit Game":
							pause = False
							self.active_state = 'Start'

			pause_menu.draw(game_display, SCREEN_CENTRE, 250, 300, 20, 20, 20, 50, 100)
			pygame.display.update()
			clock.tick(60)

	def game_loop(self):
		game_over = False
		event = None

		# main game loop
		while game_over == False:
			key = pygame.key.get_pressed()
			for event in pygame.event.get():
				if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
					pygame.quit()
					quit()
				if event.type == pygame.KEYDOWN:
					if event.key == pygame.K_q:
						game_over = True
					if event.key == pygame.K_p:
						self.pause()

			if self.old_state != self.active_state:
				self.change_state()

			self.states[self.active_state].process_event(event)	
			self.states[self.active_state].draw(game_display)
			self.old_state = self.active_state
			self.active_state = self.states[self.active_state].active_state()

			event = None
			clock.tick(60)
			pygame.display.update()