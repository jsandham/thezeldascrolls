"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame
from random import randint
from constants import *
from colors import *
from tools import *
from Component import *
from XMLParser import *


class Character(pygame.sprite.Sprite):
	"""
	The Character class is the base class for the payable character, enemies,
	and NPC's. This class takes an xml filename, an image filename, and image 
	tile size as input.  
	"""
	def __init__(self, xml_filename, image_tile_filename, image_tile_size):
		super(Character, self).__init__()
		self.image_tile_size = image_tile_size
		self.image_tile = SpriteSheet(image_tile_filename)
		self.image = self.image_tile.image_at((0, 0, 32, 32))
		self.image_dict = {}
		self.image_index = 0
		self.frame = 1
		self.screen_rect = self.image.get_rect()
		self.rect = self.image.get_rect()
		self.speed = 1
		self.x_vel = 0
		self.y_vel = 0
		self.state = 'resting'  # can be moving or resting
		self.direction = 'rest'  # direction of movement, i.e. left, right, up, down or at rest
		self.velocities = {'left': (-1,0), 'right': (1,0), 'up': (0,-1), 'down': (0,1)}
		self.make_image_dictionary()

		self.parser = XMLParser(xml_filename)
		self.sprite_dict = self.parser.convert_to_dict()

		self.screen_rect.x = self.sprite_dict['Stats'].get_item('location_x')
		self.screen_rect.y = self.sprite_dict['Stats'].get_item('location_y')
		self.rect.x = self.sprite_dict['Stats'].get_item('location_x')
		self.rect.y = self.sprite_dict['Stats'].get_item('location_y')

	def make_image_dictionary(self):
		# make dictionary of images belonging to character sprite
		down = []; up = []; left = []; right = []; rest = []; attack = []
		for i in range(0, self.image_tile_size[0]):
			if self.image_tile_size[1] >= 4:
				down.append(self.image_tile.image_at((32*i, 0, 32, 32)))
				left.append(self.image_tile.image_at((32*i, 32, 32, 32)))
				right.append(self.image_tile.image_at((32*i, 64, 32, 32)))
				up.append(self.image_tile.image_at((32*i, 96, 32, 32)))
				rest.append(self.image_tile.image_at((0, 0, 32, 32)))
				attack.append(self.image_tile.image_at((0, 0, 32, 32)))

			if self.image_tile_size[1] >= 5:
				rest.append(self.image_tile.image_at((32*i, 128, 32, 32)))
				attack.append(self.image_tile.image_at((0, 0, 32, 32)))

			if self.image_tile_size[1] == 6:
				attack.append(self.image_tile.image_at((32*i, 160, 32, 32)))

		self.image_dict = {'down': down, 'up': up, 'left': left, 'right': right, 'rest': rest, 'attack': attack}

	def name(self):
		"""
		Returns name of character.
		"""
		return self.sprite_dict['Stats'].get_item('name')

	def max_health(self):
		"""
		Returns maximum health based on characters constitution attribute.
		"""
		return self.sprite_dict['Stats'].get_item('constitution')*10

	def max_mana(self):
		"""
		Returns maximum mana based on characters intelligence attribute.
		"""
		return self.sprite_dict['Stats'].get_item('intelligence')*10

	def max_xp(self):
		"""
		Returns maximum xp based on characters level.
		"""
		return self.sprite_dict['Stats'].get_item('level')*10
 
	def health(self):
		"""
		Returns characters current health.
		"""
		return self.sprite_dict['Stats'].get_item('health')

	def mana(self):
		"""
		Returns characters current mana.
		"""
		return self.sprite_dict['Stats'].get_item('mana')

	def xp(self):
		"""
		Returns characters current xp.
		"""
		return self.sprite_dict['Stats'].get_item('xp')

	def health_fraction(self):
		"""
		Returns fraction (between 0 and 1) of characters current health to their 
		maximum health. 
		"""
		return self.health() / self.max_health()

	def mana_fraction(self):
		"""
		Returns fraction (between 0 and 1) of characters current mana to their 
		maximum mana. 
		"""
		return self.mana() / self.max_mana()

	def xp_fraction(self):
		"""
		Returns fraction (between 0 and 1) of characters current xp to their 
		maximum xp (for their current level). 
		"""
		return self.xp() / self.max_xp()

	def strength(self):
		"""
		Returns characters strength attribute.
		"""
		return self.sprite_dict['Stats'].get_item('strength')

	def agility(self):
		"""
		Returns characters agility attribute.
		"""
		return self.sprite_dict['Stats'].get_item('agility')

	def constitution(self):
		"""
		Returns characters constitution attribute.
		"""
		return self.sprite_dict['Stats'].get_item('constitution')

	def wisdom(self):
		"""
		Returns characters wisdom attribute.
		"""
		return self.sprite_dict['Stats'].get_item('wisdom')

	def intelligence(self):
		"""
		Returns characters intelligence attribute.
		"""
		return self.sprite_dict['Stats'].get_item('intelligence')

	def quests(self):
		"""
		Returns list of quest objects in the character quest log.
		"""
		return list(self.sprite_dict['Quests'].values())

	def quest_log(self):
		"""
		Returns list of quest names in the character quest log.
		"""
		return list(self.sprite_dict['Quests'].keys())

	def inventory(self):
		"""
		Returns list of item names in characters inventory.
		"""
		return list(self.sprite_dict['Inventory'].keys())

	def spells(self):
		"""
		Returns list of spell names in characters spell book.
		"""
		return list(self.sprite_dict['Spells'].keys())

	def cities(self):
		"""
		Returns list of city names in characters city log.
		"""
		return list(self.sprite_dict['Cities'].keys())

	def position(self):
		"""
		Returns character sprite location.
		"""
		return (self.old_x, self.old_y)

	def add_quest(self, quest):
		"""
		Add a quest to the character quest log.
		"""
		if self.sprite_dict['Quests'].contains(quest.title()):
			pass
		else:
			self.sprite_dict['Quests'].add(quest.title(), quest)

	def remove_quest(self, quest):
		"""
		Removes quest from the character quest log.
		"""
		self.sprite_dict['Quests'].remove(quest.title())

	def add_quests(self, quests):
		"""
		Adds a list of quests to the character quest log.
		"""
		for quest in quests:
			self.add_quest(quest)

	def remove_quests(self, quests):
		"""
		Remove list of quests from the character quest log (if they 
		are in the character quest log).
		"""
		for quest in quests:
			self.remove_quest(quest)

	def clear_quests(self):
		"""
		Clears the character quest log.
		"""
		self.sprite_dict['Quests'].empty()

	def attack(self):
		"""
		Return character weapon base attack value
		"""
		return 1 #self.sprite_dict['Weapon'].att

	def armour_penetration(self):
		"""
		Return character weapon armour penetration
		"""
		return 1 #self.sprite_dict['Weapon'].ap

	def armour(self):
		"""
		Return character armour value
		"""
		return 4 #self.sprite_dict['Armour'].armour

	def take_physical_damage(self, sprite):
		"""
		Take physical damage from another sprite. The characters health will 
		be reduced based on a comparison between the characters attributes and 
		the attacking sprites attributes. 
		"""
		sprite_weapon_attack = sprite.attack()
		sprite_weapon_ap = sprite.armour_penetration()
		sprite_strength = sprite.strength()
		sprite_agility = sprite.agility()

		armour = self.armour()

		damage = sprite_weapon_attack + sprite_strength
		damage -= armour - sprite_weapon_ap
		if damage < 0:
			damage = 0

		health = self.sprite_dict['Stats'].get_item('health') - 8*damage
		if health < 0:
			health = 0
		self.sprite_dict['Stats'].set_item('health', health)

	def take_magical_damage(self, sprite):
		"""
		Take magical damage from another sprite. The characters mana will 
		be reduced based on a comparison between the characters attributes and 
		the attacking sprites attributes. 
		"""
		pass

	def isDead(self):
		"""
		Return whether the character is alive or dead (i.e. dead if health 
		is below 0, else id alive).
		"""
		dead = False
		health = self.sprite_dict['Stats'].get_item('health')
		if health <= 0:
			dead = True
		return dead

	def reposition(self, location):
		"""
		Repositions character sprite to new location.
		"""
		self.rect.x = location[0]
		self.rect.y = location[1]
		self.image_index = 0

	def rescale(self, scale):
		"""
		Rescales character sprite size.
		"""
		self.image = pygame.transform.scale(self.image, scale)

	def redirect(self, direction):
		"""
		Changes character sprite direction (can be 'rest', 'left', 'right',
		'up' or 'down').
		"""
		self.direction = direction

	def restate(self, state):
		"""
		Changes character sprite state (can be 'moving' or 'resting').
		"""
		self.state = state

	def beside(self, character):
		"""
		Returns True if input character is beside (i.e. to the left or right or 
		above or below) current character, else returns False.
		"""
		if abs(self.rect.x - character.rect.x) + abs(self.rect.y - character.rect.y) == 32:
			return True
		else:
			return False

	def animation(self):
		"""
		Changes character sprite image based on character state and movement direction.
		"""
		if self.state == 'resting' and self.direction == 'rest':
			self.image = self.image_dict[self.direction][0]
			self.image_index = 0
			self.frame = 1
		else:
			if self.frame == 1:
				self.image = self.image_dict[self.direction][self.image_index]
				self.image_index = (self.image_index+1) % self.image_tile_size[0]
			elif self.frame == FPS/self.image_tile_size[0]:
				self.frame = 0
			self.frame += 1

	def begin_moving(self, direction):
		"""
		Begin moving character sprite based on input direction.
		"""
		if self.rect.x % 32 == 0 and self.rect.y % 32 == 0:
			self.state = 'moving'
			self.direction = direction
			self.x_vel = self.velocities[direction][0]
			self.y_vel = self.velocities[direction][1]

	def auto_moving(self, direction):
		"""
		Continue moving character sprite while character is traveling inbetween
		two 32-by-32 pixel tiles.
		"""
		self.x_vel = self.velocities[direction][0]
		self.y_vel = self.velocities[direction][1]

		self.new_tile = False
		if self.rect.x % 32 == 0 and self.rect.y % 32 ==0:
			if self.old_x != self.rect.x or self.old_y != self.rect.y:
				self.new_tile = True
				self.old_x = self.rect.x
				self.old_y = self.rect.y

	def begin_resting(self, direction):
		"""
		Begin resting character sprite by setting character state to 'resting'. Character 
		sprite will continue moving until it overlaps next 32-by-32 pixel tile. 
		"""
		self.state = 'resting'
		if self.rect.x % 32 == 0 and self.rect.y % 32 == 0:
			self.direction = 'rest'
			self.auto_resting()
		else:
			self.auto_moving(direction)

	def auto_resting(self):
		"""
		Sets character sprite old position to current position.
		"""
		self.x_vel = 0
		self.y_vel = 0

		self.new_tile = False
		if self.rect.x % 32 == 0 and self.rect.y % 32 ==0:
			if self.old_x != self.rect.x or self.old_y != self.rect.y:
				self.new_tile = True
				self.old_x = self.rect.x
				self.old_y = self.rect.y

	def update(self, event):
		"""
		Updates character...do I need this method?
		"""
		pass

	def draw(self, display, shift = (0, 0)):
		"""
		Draws character sprite to screen.
		"""
		display.blit(self.image, (self.rect.x - shift[0], self.rect.y - shift[1]))

		# print self.rect.x 
		# print self.rect.y 
		# print self.old_x
		# print self.old_y