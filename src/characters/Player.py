"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame
from Character import *


class Player(Character):
	"""
	Player class inherits from the Character class. An instance of this class 
	represents the playable character.
	"""
	def __init__(self, xml_filename, image_tile_filename, image_tile_size):
		super(Player, self).__init__(xml_filename, image_tile_filename, image_tile_size)
		self.old_x = self.rect.x
		self.old_y = self.rect.y
		self.new_tile = False
		self.fasttravel_start = None
		self.fasttravel_end = None		

	def update_quests(self, keyword):
		"""
		Update quests in player quest log based on keyword input.
		"""
		for quest in self.quests():
			quest.update(keyword)

	def turnin_quests(self, name):
		"""
		Checks if any quest in the player quest log can be turned in to the 
		input npc name. If the quest can be turned in, its experience reward 
		is given to the player and the quest is removed from the player quest 
		log.
		"""
		for quest in self.quests():
			if quest.isReturnable(name) == True:
				self.earn_xp(quest.award())
				self.remove_quest(quest)
				break

	def accept_quests(self, quests):
		"""
		Adds input quest list to players quest log.
		"""
		for quest in quests:
			self.add_quest(quest)

	def complete_quest(self, quest_title):
		"""
		Complete quest by awardig xp and removing quest from player quest log.
		"""
		quest = self.sprite_dict['Quests'].get_item(quest_title)
		self.earn_xp(quest.award())
		self.remove_quest(quest)


	def isQuestComplete(self, quest_title):
		"""
		Returns True is quest is complete, else returns False.
		"""
		quest = self.sprite_dict['Quests'].get_item(quest_title)
		return quest.isComplete()

	def activation_type(self, label):
		"""
		Returns the activation type of the item (found by using input label key) in 
		inventory. Can be either "Use", "Equip", or "Learn".
		"""
		return self.sprite_dict['Inventory'].get_item(label).activation()

	def use(self, label):
		"""
		Use item from inventory and apply its bonuses to character stats. Removes the 
		item after use.
		"""
		item = self.sprite_dict['Inventory'].remove(label)
		for stat in item.modifier_keys():
			new_stat_value = self.sprite_dict['Stats'].get_item(stat) + item.modifier(stat)
			self.sprite_dict['Stats'].set_item(stat, new_stat_value)

	def equip(self, label):
		"""
		Equip item from inventory and apply its bonuses to character stats. Removes 
		from inventory after being equiped.
		"""
		pass

	def learn(self, label):
		"""
		Learn magical spell from scroll. Learned spell added to spell book. Removes 
		from inventory once learned.
		"""
		scroll = self.sprite_dict['Inventory'].remove(label)
		self.sprite_dict['Spells'].add(scroll.name(), scroll.spell())

	def use_spell(self, label):
		"""
		Cast spell from player spell book. Spell is removed after cast.
		"""
		item = self.sprite_dict['Spells'].remove(label)
		value = self.sprite_dict['Stats'].get_item(item.attribute) - item.value
		self.sprite_dict['Stats'].set_item(item.attribute, value)

	def buy(self, label, item):
		"""
		Buy input item and place in player inventory. Player old is reduced 
		when buying.
		"""
		if self.gold() + item.cost() >= 0: 
			g = self.sprite_dict['Stats'].get_item('gold') - item.cost()
			self.sprite_dict['Stats'].set_item('gold', g)
			self.sprite_dict[item.container()].add(label, item)

	def sell(self, label):
		"""
		Sell item from player inventory. Item is removed from inventory and 
		player gold is increased.
		"""
		g = self.sprite_dict['Stats'].get_item('gold') + 1
		self.sprite_dict['Stats'].set_item('gold', g)
		item = self.sprite_dict['Inventory'].remove(label)
		if item == None:
			item = self.sprite_dict['Spells'].remove(label)
		return item

	def add_city(self, label, location):
		"""
		Add input city to player city log book.
		"""
		if self.sprite_dict['Cities'].contains(label):
			pass
		else:
			self.sprite_dict['Cities'].add(label, location)

	def fasttravel(self, start, end):
		"""
		Sets fasttravel_start and fasttravel_end variables.
		"""
		self.fasttravel_start = self.sprite_dict['Cities'].get_item(start)
		self.fasttravel_end = self.sprite_dict['Cities'].get_item(end)

	def earn_xp(self, xp):
		"""
		Adds experience points to player xp total. If xp goes above experience
		needed to level up, then player level and level points are adjusted and 
		excess player xp is carried over to new level.
		"""
		experience = self.sprite_dict['Stats'].get_item('xp') + xp
		points = self.sprite_dict['Stats'].get_item('points')
		level = self.sprite_dict['Stats'].get_item('level')
		if experience >= self.max_xp():
			experience = experience - self.max_xp()
			points += 1
			level += 1
			self.sprite_dict['Stats'].set_item('xp', experience)
			self.sprite_dict['Stats'].set_item('points', points)
			self.sprite_dict['Stats'].set_item('level', level)
		else:
			self.sprite_dict['Stats'].set_item('xp', experience)

	def earn_gold(self, gold):
		"""
		Adds input gold to player gold total.
		"""
		g = self.sprite_dict['Stats'].get_item('gold') + gold
		self.sprite_dict['Stats'].set_item('gold', g)

	def gold(self):
		"""
		Returns player gold total.
		"""
		return self.sprite_dict['Stats'].get_item('gold')

	def level(self):
		"""
		Returns player level.
		"""
		return self.sprite_dict['Stats'].get_item('level')

	def points(self):
		"""
		Return player level points.
		"""
		return self.sprite_dict['Stats'].get_item('points')

	def levelup(self, attribute):
		"""
		Level up player attribute (provided a level point is available).
		"""
		if self.points() > 0:
			att = self.sprite_dict['Stats'].get_item(attribute) + 1
			points = self.sprite_dict['Stats'].get_item('points') - 1
			self.sprite_dict['Stats'].set_item(attribute, att)
			self.sprite_dict['Stats'].set_item('points', points)

	def isLeveled(self):
		"""
		Return True is player can level up, else False.
		"""
		if self.points() > 0:
			return True
		else:
			return False

	def reset_coordinates(self):
		"""
		Reset player sprite coordinates to previous old player corrdinates.
		"""
		self.rect.x = self.old_x
		self.rect.y = self.old_y

	def animate_shop(self, flag):
		"""
		Sets/changes flags dictating player animation in shop states.
		"""
		if flag == "enter":
   			self.reposition((200,200))
   			flag = "walk right"
		elif flag == "leave":
			flag = "walk left"
		elif flag == "walk right":
			self.redirect('right')
			if self.rect.x < 384:
				self.x_vel = self.velocities['right'][0]
			else:
				self.direction = 'rest'
				self.x_vel = 0
		elif flag == "walk left":
			self.redirect('left')
			if self.rect.x > 200:
  				self.x_vel = self.velocities['left'][0]
			else:
				self.reposition((self.old_x, self.old_y))
				flag = "exit shop"
				self.direction = 'rest'
				self.state = 'resting'
				self.x_vel = 0

		self.rect.x += self.x_vel
		self.animation()
		self.rescale((128, 128))
		return flag

	def animate_battle(self, flag):
		"""
		Sets/changes flags dictating player animation in battle state.
		"""
		if flag == "enter":
			self.reposition((50, 200))
			flag = "walk right"
		elif flag == "leave":
			flag = 'walk left'
		elif flag == "walk right":
			self.redirect('right')
			if self.rect.x < 200:
				self.x_vel = self.velocities['right'][0]
			else:
				self.direction = 'rest'
				self.x_vel = 0
		elif flag == 'walk left':
			self.redirect('left')
			if self.rect.x > 60:
				self.x_vel = self.velocities['left'][0]
			else:
				self.reposition((self.old_x, self.old_y))
				flag = "exit battle"
				self.direction = 'rest'
				self.state = 'resting'
				self.x_vel = 0
		elif flag == "start attack":
			self.redirect('right')
			if self.rect.x < 250:
				self.x_vel = self.velocities['right'][0]
			else:
				flag = "end attack"
		elif flag == "end attack":
			if self.rect.x > 200:
				self.x_vel = self.velocities['left'][0]
			else:
				self.direction = 'rest'
				self.x_vel = 0
				flag = "enemy start attack"

		# if self.sprite_dict['Health'] == 0:
		if self.isDead():
			flag = "player died"

		self.rect.x += self.x_vel
		self.animation()
		self.rescale((128, 128))
		return flag

	def animate_stats(self, flag):
		"""
		Sets/changes flags dictating player animation in stat screen state.
		"""
		if flag == "leave":
			self.reposition((self.old_x, self.old_y))
			flag = "exit"
		elif flag == "enter":
			self.reposition((0, 0))
			flag = ""

		self.animation()
		self.rescale((256, 256))
		return flag

	def handle_event(self, event):
		"""
		Begin moving or resting of player sprite based on input events.
		"""
		if event != None:
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_LEFT:
					self.begin_moving('left')
				elif event.key == pygame.K_RIGHT:
					self.begin_moving('right')
				elif event.key == pygame.K_UP:
					self.begin_moving('up')
				elif event.key == pygame.K_DOWN:
					self.begin_moving('down')

			elif event.type == pygame.KEYUP:
				self.begin_resting(self.direction)

	def handle_state(self):
		"""
		Continue moving or begin resting based on player sprite state.
		"""
		if self.state == 'resting':
			self.begin_resting(self.direction)
		elif self.state == 'moving':
			self.auto_moving(self.direction)

	def update(self, event):
		"""
		Update player sprite movement based on user input events.
		"""
		self.handle_event(event)
		self.handle_state()
		self.animation()

		self.rect.x += self.x_vel
		self.rect.y += self.y_vel