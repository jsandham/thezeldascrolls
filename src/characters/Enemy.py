"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame
import random
from Character import *


class Enemy(Character):
	"""
	Enemy class inherits from the Character class. All enemy characters in the game 
	are instances of this class.
	"""
	def __init__(self, xml_filename, image_tile_filename, image_tile_size, box = None):
		super(Enemy, self).__init__(xml_filename, image_tile_filename, image_tile_size)
		self.old_x = self.rect.x
		self.old_y = self.rect.y
		self.new_tile = False
		self.enemy_paused = True
		self.enemy_moving = False
		self.enemy_talking = False
		self.bounding_box = box 
		self.tiles_moved = 0
		self.frames_paused = 0

	def animate_battle(self, flag):
		"""
		Move enemy sprite during battles based on input flags.
		"""
		if flag == "enemy start attack":
			self.redirect('left')
			if self.rect.x > 350:
				self.x_vel = self.velocities['left'][0]
			else:
				flag = "enemy end attack"
		elif flag == "enemy end attack":
			if self.rect.x < 400:
				self.x_vel = self.velocities['right'][0]
			else:
				self.direction = 'rest'
				self.x_vel = 0
				flag = "attack phase over"

		self.rect.x += self.x_vel
		self.animation()
		self.rescale((128, 128))
		return flag

	def handle_random_walk(self):
		"""
		Move enemy sprite (randomly walking around) in world states based on 
		frames passed since last movement.
		"""
		if self.enemy_paused:
			if self.frames_paused == 200:
				self.enemy_paused = False
				self.enemy_moving = True
				self.frames_paused = 0
				rand = random.randint(1,4)
				rand = self.check_bounding_box(rand)
				if rand == 1:
					self.begin_moving('left')
				elif rand == 2:
					self.begin_moving('right')
				elif rand == 3:
					self.begin_moving('up')
				else:
					self.begin_moving('down')
			else:
				if self.enemy_talking == False:
					self.frames_paused += 1

		elif self.enemy_moving:
			if self.tiles_moved == 1:
				self.enemy_paused = True
				self.enemy_moving = False
				self.tiles_moved = 0
				self.begin_resting(self.direction)
			else:
				if self.new_tile == True:
					self.tiles_moved += 1

	def handle_state(self):
		"""
		Continue moving or begin resting based on enemy sprite state.
		"""
		if self.state == 'resting':
			self.begin_resting(self.direction)
		elif self.state == 'moving':
			self.auto_moving(self.direction)

	def update(self, event):
		"""
		Update enemy sprite movement based on input events.
		"""
		self.handle_random_walk()
		self.handle_state()
		self.animation()

		self.rect.x += self.x_vel
		self.rect.y += self.y_vel

	def check_bounding_box(self, n):
		"""
		Used when enemy sprite is randomly walking in world state, this method
		returns direction (based partially on input) so that enemy sprite 
		remains within bounding box.
		"""
		future_x = self.rect.x
		future_y = self.rect.y
		if n == 1:
			self.future_x = self.rect.x - 32*2
		elif n ==2:
			self.future_x = self.rect.x + 32*2
		elif n == 3:
			self.future_y = self.rect.y - 32*2
		else:
			self.future_y = self.rect.y + 32*2

		direction = n
		if n == 1:
			if self.future_x <= self.bounding_box.x:
				direction = 2
		elif n == 2:
			if self.future_x >= self.bounding_box.x + self.bounding_box.width:
				direction = 1
		elif n == 3:
			if self.future_y <= self.bounding_box.y:
				direction = 4
		elif n == 4:
			if self.future_y >= self.bounding_box.y + self.bounding_box.height:
				direction = 3

		return direction