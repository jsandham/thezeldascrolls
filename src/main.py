"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame
from setup import *
from Game import *


def main():
	game_instance = Game()
	game_instance.set_states(game_states)
	game_instance.game_loop()
