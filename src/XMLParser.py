"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame
import xml.etree.ElementTree as ET

from Container import *
from Potion import *
from Weapon import *
from Armour import *
from Scroll import *
from Quest import *

"""
XMLParser takes an input xml filename and enables the conversion 
of the data to a python dictionary used in all character classes.
Also allows for extraction of xml dialogue tree for use in 
DialogBox components.
"""


class XMLParser():
	"""
	XMLParser allows for the parsing of character xml data files.
	"""
	def __init__(self, filename):
		self.tree = ET.parse(filename)
		self.root = self.tree.getroot()

	def get_dialog_tree(self):
		"""
		Return xml character dialog tree.
		"""
		temp = self.root.find('dialogue_tree') 
		return temp

	def element_text(self, item, tag, default):
		etext = item.find(tag)
		if etext == None:
			return default
		else:
			return etext.text

	def convert_to_dict(self):
		"""
		Convert character xml file to more usuable python character dictionary. This
		character dictioary contains data on the characters stats, spells, inventory,
		and quests.
		"""
		dictionary = {'Stats': Container(),
					  'Spells': Container(),
					  'Inventory': Container(),
					  'Quests': Container(),
					  'Cities': Container()}

		# parse character stats
		stats = self.root.find('stats')
		if stats != None:
			for stat in stats:
				if stat.tag == 'name':
					dictionary['Stats'].add(stat.tag, stat.text)
				else:
					dictionary['Stats'].add(stat.tag, int(stat.text))

		# parse character inventory items
		inventory = self.root.find('inventory')
		if inventory != None:
			for item in inventory:
				filename = self.element_text(item, 'filename', '')
				container = self.element_text(item, 'container', '')
				activation = self.element_text(item, 'activation', '')
				name = self.element_text(item, 'name', '')
				description = self.element_text(item, 'description', '')
				cost = int(self.element_text(item, 'cost', 0))
				weight = int(self.element_text(item, 'weight', 0))
				level = int(self.element_text(item, 'level', 0))

				health = int(self.element_text(item, 'health', 0))
				mana = int(self.element_text(item, 'mana', 0))
				strength = int(self.element_text(item, 'strength', 0))
				agility = int(self.element_text(item, 'agility', 0))
				constitution = int(self.element_text(item, 'constitution', 0))
				wisdom = int(self.element_text(item, 'wisdom', 0))
				intelligence = int(self.element_text(item, 'intelligence', 0))
				defense = int(self.element_text(item, 'defense', 0))

				modifiers = {'health': health,
							 'mana': mana,
							 'strength': strength,
							 'agility': agility,
							 'constitution': constitution,
							 'wisdom': wisdom,
							 'intelligence': intelligence,
							 'defense': defense}

				if item.attrib['type'] == 'potion':
					potency = int(self.element_text(item, 'potency', 0))
					dictionary['Inventory'].add(name, Potion(filename, container, activation, name, description, cost, weight, level, modifiers, potency))
				elif item.attrib['type'] == 'weapon':
					power = int(self.element_text(item, 'power', 0))
					dictionary['Inventory'].add(name, Weapon(filename, container, activation, name, description, cost, weight, level, modifiers, power))
				elif item.attrib['type'] == 'armour':
					protection = int(self.element_text(item, 'protection', 0))
					dictionary['Inventory'].add(name, Armour(filename, container, activation, name, description, cost, weight, level, modifiers, protection))
				elif item.attrib['type'] == 'scroll':
					intensity = int(self.element_text(item, 'intensity', 0))
					dictionary['Inventory'].add(name, Scroll(filename, container, activation, name, description, cost, weight, level, modifiers, intensity))

		# parse character spells
		spells = self.root.find('spells')
		if spells != None:
			for spell in spells:
				pass

		# parse character quests
		quests = self.root.find('quests')
		if quests != None:
			for quest in quests:
				title = self.element_text(quest, 'title', '').lstrip().rstrip()
				description = self.element_text(quest, 'description', '').lstrip().rstrip()
				issued_by = self.element_text(quest, 'issued_by', '')
				returned_to = self.element_text(quest, 'returned_to', '')
				reward = int(self.element_text(quest, 'reward', ''))
				keyword = self.element_text(quest, 'keyword', '')
				count = int(self.element_text(quest, 'count', ''))
				dictionary['Quests'].add(title, Quest(title, description, issued_by, returned_to, reward, keyword, count))

		return dictionary