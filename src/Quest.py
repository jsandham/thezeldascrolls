"""
The Zelda Scrolls is a two-dimensional "top-down" rpg game written in 
Python using pygame. The source code is open source and can be used by 
anyone. For questions/comments/concerns I can be contacted at 
jsandham@uwaterloo.ca. 
"""

__author__ = "James Sandham"

import pygame
from setup import *

"""
Quests in The Zelda Scrolls operate by the use of keywords (i.e. strings). Each
quest has a keyword string associated with it. For example if the quest is to talk 
to a NPC named 'Bob', than the keyword for this quest might be 'Bob'. The quest 
remains uncomplete until its keyword has been passed (through the update method) 
count times. Continuing with the previous example, the quest is to talk to NPC named 
'Bob'. Everytime we talk to an NPC we call the quest update method and pass 
the NPC's name as the argument. If the NPC's name happens to be 'Bob', then 
because this matches the quests keyword, the quest is then set to complete. In 
this case we only had to pass the keyword 'Bob' one time before the quest was 
marked complete. For other types of quests you may need to pass the keyword more
than once before the quest will be marked as complete. For example lets say the 
quest is to kill 4 dragons. In this case the quests keyword might be 'dragon' 
and count=4. Now everytime we kill an enemy we again call the quests update method
and pass the enemies type as the argument. If we happen to kill a dragon than 
count is reduced by one. Once we have killed 4 dragons, count will be reduced to zero 
and the quest will be marked as complete.

All quests have both an issuer variable, the NPC's name who gave the quest, and a 
return_to variable, the NPC's name who the player should return the quest to. 
The reward will be awarded to the player once a quest that is marked as complete is 
returned to the return_to NPC.  
"""


class Quest(object):
	def __init__(self, title, description, issued_by, return_to, reward, keyword, count = 1):
		self.complete = False
		self.quest_title = title
		self.quest_description = description
		self.issued_by = issued_by
		self.return_to = return_to
		self.reward = reward
		self.keyword = keyword
		self.count = count

	def isComplete(self):
		return self.complete

	def isReturnable(self, name):
		if self.return_to == name and self.complete == True:
			return True
		else:
			return False

	def title(self):
		return self.quest_title

	def description(self):
		return self.quest_description

	def award(self):
		return self.reward

	def update(self, word):
		if self.count > 0:
			if self.keyword == word:
				self.count -= 1
		if self.count == 0:
			self.complete = True